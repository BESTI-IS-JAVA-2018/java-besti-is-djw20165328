import static org.junit.Assert.*;
import org.junit.Test;

public class MyDocTest {
    @Test
    public void MyDoc(){

        MyDoc num1 = new MyDoc();
        assert num1.getA() == 0 : "error1:MyDoc()";
        assert num1.getA() == 0 : "error2:MyDoc()";

        MyDoc num2 = new MyDoc(0, 0);
        assert num2.getA() == 0 : "error3:MyDoc(double a,double b)";
        assert num2.getB() == 0 : "error4:MyDoc(double a,double b)";

        MyDoc num3 = new MyDoc();
        num3.setA(0);
        assert num3.getA() == 0 : "error7:setA(double a)";

        num3.setB(0);
        assert num3.getB() == 0 : "error8:setB(double b)";

        MyDoc num4 = new MyDoc();
        MyDoc num5 = new MyDoc(-1, -1);
        MyDoc num6 = new MyDoc();
        num4 = num1.MyDocAdd(num1);
        assert num4.getA() == 0 : "error9:MyDocAdd(MyDoc c)";
        assert num4.getB() == 0 : "error10:MyDocAdd(MyDoc c)";
        num6 = num1.MyDocAdd(num5);
        assert num6.getA() == -1 : "error11:MyDocAdd(MyDoc c)";
        assert num6.getB() == -1 : "error12:MyDocAdd(MyDoc c)";

        MyDoc num7 = new MyDoc();
        MyDoc num8 = new MyDoc(1, 1);
        MyDoc num9 = new MyDoc();
        num7 = num1.MyDocMinus(num1);
        assert num7.getA() == 0 : "error13:MyDocMinus(MyDoc c)";
        assert num7.getB() == 0 : "error14:MyDocMinus(MyDoc c)";
        num9 = num1.MyDocMinus(num8);
        assert num9.getA() == 1 : "error13:MyDocMinus(MyDoc c)";
        assert num9.getB() == 1 : "error14:MyDocMinus(MyDoc c)";

        MyDoc num10 = new MyDoc();
        MyDoc num11 = new MyDoc(-1, -1);
        MyDoc num12 = new MyDoc();
        MyDoc num13 = new MyDoc();
        MyDoc num14 = new MyDoc(1, 1);
        num10 = num1. MyDocMulti(num1);
        assert num10.getA() == 0 : "error15:MyDocMulti( MyDoc c)";
        assert num10.getB() == 0 : "error16:MyDocMulti( MyDoc c)";
        num12 = num1. MyDocMulti(num11);
        assert num12.getA() == 0 : "error17:MyDocMulti( MyDoc c)";
        assert num12.getB() == 0 : "error18:MyDocMulti( MyDoc c)";
        num13 = num11. MyDocMulti(num14);
        assert num13.getA() == -1 : "error19： MyDocMulti( MyDoc c)";
        assert num13.getB() == -1 : "error20: MyDocMulti( MyDoc c)";

        System.out.println("Pass!");
    }
}

public class MyDocTest {
    @Test
    public void MyDoc(){

        MyDoc num1 = new MyDoc();
        assert num1.getA() == 0 : "error1:MyDoc()";
        assert num1.getA() == 0 : "error2:MyDoc()";

        MyDoc num2 = new MyDoc(0, 0);
        assert num2.getA() == 0 : "error3:MyDoc(double a,double b)";
        assert num2.getB() == 0 : "error4:MyDoc(double a,double b)";

        MyDoc num3 = new MyDoc();
        num3.setA(0);
        assert num3.getA() == 0 : "error7:setA(double a)";

        num3.setB(0);
        assert num3.getB() == 0 : "error8:setB(double b)";

        MyDoc num4 = new MyDoc();
        MyDoc num5 = new MyDoc(-1, -1);
        MyDoc num6 = new MyDoc();
        num4 = num1.MyDocAdd(num1);
        assert num4.getA() == 0 : "error9:MyDocAdd(MyDoc c)";
        assert num4.getB() == 0 : "error10:MyDocAdd(MyDoc c)";
        num6 = num1.MyDocAdd(num5);
        assert num6.getA() == -1 : "error11:MyDocAdd(MyDoc c)";
        assert num6.getB() == -1 : "error12:MyDocAdd(MyDoc c)";

        MyDoc num7 = new MyDoc();
        MyDoc num8 = new MyDoc(1, 1);
        MyDoc num9 = new MyDoc();
        num7 = num1.MyDocMinus(num1);
        assert num7.getA() == 0 : "error13:MyDocMinus(MyDoc c)";
        assert num7.getB() == 0 : "error14:MyDocMinus(MyDoc c)";
        num9 = num1.MyDocMinus(num8);
        assert num9.getA() == 1 : "error13:MyDocMinus(MyDoc c)";
        assert num9.getB() == 1 : "error14:MyDocMinus(MyDoc c)";

        MyDoc num10 = new MyDoc();
        MyDoc num11 = new MyDoc(-1, -1);
        MyDoc num12 = new MyDoc();
        MyDoc num13 = new MyDoc();
        MyDoc num14 = new MyDoc(1, 1);
        num10 = num1. MyDocMulti(num1);
        assert num10.getA() == 0 : "error15:MyDocMulti( MyDoc c)";
        assert num10.getB() == 0 : "error16:MyDocMulti( MyDoc c)";
        num12 = num1. MyDocMulti(num11);
        assert num12.getA() == 0 : "error17:MyDocMulti( MyDoc c)";
        assert num12.getB() == 0 : "error18:MyDocMulti( MyDoc c)";
        num13 = num11. MyDocMulti(num14);
        assert num13.getA() == -1 : "error19： MyDocMulti( MyDoc c)";
        assert num13.getB() == -1 : "error20: MyDocMulti( MyDoc c)";

        System.out.println("Pass!");
    }
}

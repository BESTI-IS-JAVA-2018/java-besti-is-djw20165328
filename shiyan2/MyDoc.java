import java.util.Scanner;
import java.lang.String;

public class MyDoc{
    private double a;
    private double b;

    public MyDoc(){
        this.a = 0;
        this.b = 0;
    }

    public MyDoc(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA(){
        return this.a;
    }
    public double getB(){
        return this.b;
    }

    public double setA(double a){
        this.a = a;
        return a;
    }
    public double setB(double b){
        this.b = b;
        return b;
    }

    MyDoc MyDocAdd(MyDoc c){
        double a = c.getA();
        double b = c.getB();
        double new_a = a + this.a;
        double new_b = b + this.b;
        return new MyDoc(new_a, new_b);
    }

    MyDoc MyDocMinus(MyDoc c){
        double a = c.getA();
        double b = c.getB();
        double new_a = a - this.a;
        double new_b = b - this.b;
        return new MyDoc(new_a, new_b);
    }

    MyDoc MyDocMulti(MyDoc c){
        double a = c.getA();
        double b = c.getB();
        double new_a = a * this.a;
        double new_b = b * this.b;
        return new MyDoc(new_a, new_b);
    }
    MyDoc MyDocDiv(MyDoc c){
        double a = c.getA();
        double b = c.getB();
        double new_a = (a * c.b + b *c.a) / (c.b * c.b + c.a * c.a);
        double new_b = (b * c.b + a * c.a) / (c.b * c.b + c.a * c.a);
        return new MyDoc(new_a, new_b);
    }

    public String toString(double a,double b){
        String cnum = " ";
        if(b == 0) {
            cnum = "" + a;
        }
        else if(b < 0) {
            cnum = a + b + "i";
        }
        else if(b > 0){
            cnum = a + "+" + b +"i";
        }
        return cnum;
    }
}

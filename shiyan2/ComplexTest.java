import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a=new Complex(1,2);
    Complex b=new Complex(-2,-1);
    Complex c=new Complex(4,-2);
    Complex d=new Complex(4,-2);
    @Test
    public void testequals(){
        assertEquals(false,a.equals(b));
        assertEquals(false,b.equals(c));
        assertEquals(true,c.equals(d));
    }

    @Test
    public void testAdd(){
        assertEquals(new Complex(-1,1),a.ComplexAdd(b));
        assertEquals(new Complex(5,0),a.ComplexAdd(c));
    }

    @Test
    public void testSub(){
        assertEquals(new Complex(3,3),a.ComplexSub(b));
        assertEquals(new Complex(-3,4),a.ComplexSub(c));
    }

    @Test
    public void testMulti(){
        assertEquals(new Complex(0,-5),a.ComplexMulti(b));
        assertEquals(new Complex(8,6),a.ComplexMulti(c));
    }

    @Test
    public void testDiv(){
        assertEquals(new Complex(0,0.5),a.ComplexDiv(c));
        assertEquals(new Complex(-0.3,-0.4),b.ComplexDiv(c));
    }
}

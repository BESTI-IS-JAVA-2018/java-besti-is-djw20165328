import java.util.Scanner;
public class Zuheshu {
    public static void main(String[] args) {
        System.out.println("请输入n、m的值:");
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int m=scanner.nextInt();
        int result=zuheshu(n,m);
        System.out.println("组合数的结果为"+result);
        scanner.close();
    }
    public static int jisuan(int n) {
        if(n==1||n==0)
            return 1;
        else
            return jisuan(n-1)*n;
    }
    public static int zuheshu(int x,int y) {
        int a=jisuan(x);
        int b=jisuan(y);
        int c=jisuan(x-y);
        return a/(b*c);
    }
}

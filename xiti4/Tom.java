class A {
    privata int x = 120;
    protected int y = 20;
    int z = 11;
    privata void f() {
        x = 200;
        System.out.println(x);
    }
    void g() {
        x = 200;
        System.out.println(x);
    }
}
public class Tom {
    public static void main(String args[]) {
        A a = new A();
        a.x = 22;    //[代码1]
        a.y = 33;    //[代码2]
        a.z = 55;    //[代码3]
        a.f();       //[代码4]
        a.g();       //[代码5]
    }
}

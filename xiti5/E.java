class A {
    double f(double x,double y) {
        return x+y;
    }
}
class B extends A {
    double f(int x,int y) {
        return x*y;
    }
}
public class E {
    public static void mian(String[] args) {
        B b=new B();
        System.out.println(b.f(3,5));        //[代码1]
        System.out.println(b.f(3.0,5.0));    //[代码2]
    }
}

class A {
    int m;
    int getM() {
        return m;
    }
    int seeM() {
        return m;
    }
}
class B extends A {
    int m;
    int getM() {
        return m+100;
    }
}
public class E2 {
    public static void main(String args[]) {
        B b = new B();
        b.m = 20;
        System.out.println(b.getM());    //[代码1]
        A a = b;
        a.m = -100;                    
        System.out.println(a.getM());    //[代码2]
        System.out.println(b.seeM());    //[代码3]
    }
}
